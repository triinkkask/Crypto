package ee.bcs.valiit;

import java.io.IOException;
import java.util.Iterator;

public class DateBasedEncryptor extends Cyptor {

    public DateBasedEncryptor(String text) throws IOException {
        super(text);
    }

    protected String encryptor(String text, String coded_text) throws IOException {
        StringBuilder encrypterFinal = new StringBuilder();
        Cyptor crypter = new Cyptor(text);
        for (Iterator<String> it = crypter.readFile(text).keySet().iterator(); it.hasNext(); ) {
            String f = it.next();
            for (int i = 0; i < coded_text.length() - 1; i++) {
                if (coded_text.charAt(i) == f.charAt(0)) {
                    encrypterFinal.append(crypter.readFile(text).get(f));
                }
            }
        }
            return encrypterFinal.toString();
} }

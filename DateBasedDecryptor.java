package ee.bcs.valiit;

import java.io.IOException;
import java.util.Iterator;

public class DateBasedDecryptor extends Cyptor {

    public DateBasedDecryptor(String text) throws IOException {
        super(text);
    }

    protected String decryptor(String text, String decoded_line) throws IOException {
        StringBuilder encrypterFinal = new StringBuilder();
        Cyptor crypter = new Cyptor(text);
        for (Iterator<String> it = crypter.readFile(text).keySet().iterator(); it.hasNext(); ) {
            for(int i=0; i<text.length(); i++) {
                char[] f = it.next().toCharArray();
                if (decoded_line.charAt(i) == f[i]) {
                    encrypterFinal.append(crypter.readFile(text).get(f[i]));
                }
            }
        }
        return encrypterFinal.toString();
    }
    }
